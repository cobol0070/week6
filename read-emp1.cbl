       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-EMP1.
       AUTHOR.   WIMONSIRI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "EMP1.DAT"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD EMP-FILE.
       01 EMP-DATAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE.
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-Y0B PIC 9(4).
              10 EMP-M0B PIC 9(2).
              10 EMP-DOB PIC 9(2).
           05 EMP-GENDER PIC X.



       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT EMP-FILE
           PERFORM UNTIL END-OF-EMP-FILE 
              READ EMP-FILE 
                 AT END SET END-OF-EMP-FILE TO TRUE
              END-READ
              IF NOT END-OF-EMP-FILE  THEN
                 DISPLAY "--------------------------------------------"
                 DISPLAY "SSN: " EMP-SSN 
                 DISPLAY "NAME: " EMP-SURNAME " " EMP-FORENAME
                 DISPLAY "DOB: " EMP-Y0B  "/" EMP-M0B "/" EMP-DOB
                 DISPLAY "GENDER: " EMP-GENDER 
           END-PERFORM
           CLOSE EMP-FILE 

           GOBACK 
           .